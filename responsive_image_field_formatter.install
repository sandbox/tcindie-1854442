<?php
/**
 * @file responsive_image_field_formatter.install
 */

/**
 * Implements hook_requirements().
 */
function responsive_image_field_formatter_requirements($phase) {
  if ($phase == 'runtime') {
    $t = get_t();
    $enquire = libraries_detect('enquire.js');
    if (!$enquire['installed']) {
      $requirements['enquire.js'] = array(
        'title' => $t('Enquire.js Library'),
        'description' => $t('The Enquire.js library was not found. Please download it from !download_url and install into your libraries directory.', array('!download_url' => l($enquire['download url'], $enquire['download url']))),
        'severity' => REQUIREMENT_WARNING,
      );
    }
    elseif ($enquire['installed']) {
      $requirements['enquire.js'] = array(
        'title' => $t('Enquire.js Library'),
        'value' => $enquire['version'],
        'severity' => REQUIREMENT_OK,
      );
    }
    $matchMedia = libraries_detect('matchMedia.js');
    if (!$matchMedia['installed'] && !variable_get('riff_hide_matchmedia_warning', FALSE)) {
      $requirements['matchMedia.js'] = array(
        'title' => $t('matchMedia.js Polyfill Library'),
        'description' => $t('The matchMedia.js polyfill library was not found. Without this, the Responsive Image Field Formatter may not work on legacy browsers.<br>Refer to !caniuse to determine which browsers are supported without it.', array('!caniuse' => l('this table', 'http://caniuse.com/matchmedia'))),
        'severity' => REQUIREMENT_WARNING,
      );
    }
    elseif ($matchMedia['installed']) {
      $requirements['matchMedia.js'] = array(
        'title' => $t('matchMedia.js Polyfill Library'),
        'value' => 'Found',
        'severity' => REQUIREMENT_OK,
      );
    }
    return $requirements;
  }
}

/**
 * Implemnents hook_install().
 */
function responsive_image_field_formatter_install() {
  $default_breakpoints = array(
    array(
      'breakpoint' => 'Mobile Portrait',
      'machine_name' => 'mobile-portrait',
      'media_query' => 'screen and (max-width : 479px)',
      'weight' => -10,
    ),
    array(
      'breakpoint' => 'Mobile Landscape',
      'machine_name' => 'mobile-landscape',
      'media_query' => 'screen and (min-width : 480px) and (max-width : 599px)',
      'weight' => -9,
    ),
    array(
      'breakpoint' => 'Tablet Portrait',
      'machine_name' => 'tablet-portrait',
      'media_query' => 'screen and (min-width : 600px) and (max-width : 768px)',
      'weight' => -8,
    ),
    array(
      'breakpoint' => 'Tablet Landscape',
      'machine_name' => 'tablet-landscape',
      'media_query' => 'screen and (min-width : 769px) and (max-width : 1024px)',
      'weight' => -7,
    ),
    array(
      'breakpoint' => 'Desktop',
      'machine_name' => 'desktop',
      'media_query' => 'screen and (min-width : 1025px)',
      'weight' => -6,
    ),
  );
  variable_set('riff_enquire_variant', 'production');
  variable_set('riff_enquire_listen_time', 500);
  variable_set('riff_breakpoints', $default_breakpoints);
}

/**
 * Implemnents hook_uninstall().
 */
function responsive_image_field_formatter_uninstall() {
  variable_del('riff_breakpoints');
  variable_del('riff_enquire_variant');
  variable_del('riff_enquire_listen_time');
}