Installation
-----------------------
1. Download the Enquire.js file from:
   https://github.com/WickyNilliams/enquire.js/archive/master.zip
2. Extract the zipfile in your libraries directory so that the directory
   structure is [libraries]/enquire.js/ and the enquire.js and enquire.min.js
   files are located in [libraries]/enquire.js/dist/

To add support for legacy browsers, the matchMedia polyfill library is
necessary. If your theme already includes this, the warning on the status
report can be ignored. Otherwise, if you install it in your libraries
directory it will be loaded automatically.

How to install the matchMedia polyfill library
----------------------------------------------
1. Download the zipfile or clone the repository from GitHub:
   https://github.com/paulirish/matchMedia.js/
2. Extract, or clone into [libraries]/matchMedia.js
3. Ensure the javascript file is accessible at:
   [libraries]/matchMedia.js/matchMedia.js