<?php

/**
 * @file responsive_image_field_formatter.admin.inc
 */


/**
 * System settings form.
 */
function responsive_image_field_formatter_system_settings_form($form, &$form_state) {
  if (isset($form_state['breakpoint_count'])) {
    $breakpoints = $form_state['values']['riff_breakpoints'];
    $breakpoint_count = $form_state['breakpoint_count'];
  }
  else {
    $breakpoints = variable_get('riff_breakpoints', array());
    $breakpoint_count = (count($breakpoints));
    $form_state['breakpoint_count'] = $breakpoint_count;
  }

  $form['riff_breakpoints'] = array(
    '#type' => 'fieldset',
    '#title' => t('Breakpoints'),
    '#description' => t('Configure your desired media query breakpoints'),
    '#tree' => TRUE,
    '#prefix' => '<div id="riff-breakpoints-wrapper">',
    '#suffix' => '</div>',
    '#theme' => 'responsive_image_field_formatter_system_settings',
  );
  for ($i = 0; $i < $breakpoint_count; $i++) {
    $form['riff_breakpoints'][$i]['breakpoint'] = array(
      '#type' => 'textfield',
      '#title' => t('Breakpoint Name'),
      '#size' => 24,
      '#default_value' => isset($breakpoints[$i]) ? $breakpoints[$i]['breakpoint'] : '',
    );
    $form['riff_breakpoints'][$i]['machine_name'] = array(
      '#title' => t('Breakpoint Machine Name'),
      '#description' => t('A unique machine-readable name. Can only contain lowercase letters, numbers and dashes.'),
      '#type' => 'machine_name',
      '#default_value' => isset($breakpoints[$i]) ? $breakpoints[$i]['machine_name'] : '',
      '#machine_name' => array(
        'exists' => 'responsive_image_field_formatter_unique_machinename',
        'source' => array('riff_breakpoints', $i, 'breakpoint'),
        'replace_pattern' => '[^a-z0-9-]+',
        'replace' => '-',
      ),
    );
    $form['riff_breakpoints'][$i]['media_query'] = array(
      '#title' => t('Media Query'),
      '#type' => 'textfield',
      '#size' => 100,
      '#default_value' => isset($breakpoints[$i]) ? $breakpoints[$i]['media_query'] : '',
    );
    $form['riff_breakpoints'][$i]['weight'] = array(
      '#title' => t('Weight'),
      '#type' => 'weight',
      '#default_value' => isset($breakpoints[$i]) ? $breakpoints[$i]['weight'] : 0,
      '#attributes' => array(
        'class' => array(
          'breakpoint-weight'
        ),
      ),
    );
  }
  $form['riff_breakpoints']['add'] = array(
    '#type' => 'submit',
    '#value' => t('Add Another'),
    '#submit' => array('responsive_image_field_formatter_settings_add_breakpoint'),
    '#limit_validation_errors' => array(),
    '#ajax' => array(
      'callback' => 'responsive_image_field_formatter_settings_add_remove_breakpoint_callback',
      'wrapper' => 'riff-breakpoints-wrapper',
    ),
  );
  if ($form_state['breakpoint_count'] > 1) {
    $form['riff_breakpoints']['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Remove Last'),
      '#limit_validation_errors' => array(),
      '#submit' => array('responsive_image_field_formatter_settings_remove_breakpoint'),
      '#ajax' => array(
        'callback' => 'responsive_image_field_formatter_settings_add_remove_breakpoint_callback',
        'wrapper' => 'riff-breakpoints-wrapper',
      ),
    );
  }
  $form['enquire'] = array(
    '#type' => 'fieldset',
    '#title' => t('Enquire.js Library Settings'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#tree'  => FALSE,
  );
  $form['enquire']['riff_enquire_variant'] = array(
    '#type' => 'radios',
    '#title' => t('Which Enquire library to use'),
    '#description' => t('This should never need to be changed, Minified Production mode is preferred.'),
    '#options' => array(
      'development' => t('Development'),
      'production' => t('Production (minified)'),
    ),
    '#default_value' => variable_get('riff_enquire_variant', 'production'),
  );
  $form['enquire']['riff_enquire_listen_time'] = array(
    '#type' => 'textfield',
    '#title' => t('Enquire.js listen delay'),
    '#size' => 8,
    '#description' => t('Events are throttled so that they are only fired if there have been no resize or orientation events in the amount of time specified here.'),
    '#field_suffix' => 'ms',
    '#default_value' => variable_get('riff_enquire_listen_time', 500),
  );
  $form['#submit'][] = 'responsive_image_field_formatter_cache_flush';

  $matchMedia = libraries_detect('matchMedia.js');
  if (!$matchMedia['installed']) {
    $form['matchmedia'] = array(
      '#type' => 'fieldset',
      '#title' => t('matchMedia.js'),
      '#tree' => FALSE,
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );
    $form['matchmedia']['riff_hide_matchmedia_warning'] = array(
      '#type' => 'checkbox',
      '#title' => t('Hide matchMedia.js warning on status report'),
      '#default_value' => variable_get('riff_hide_matchmedia_warning', FALSE),
    );
  }
  return system_settings_form($form);
}

function responsive_image_field_formatter_cache_flush() {
  field_info_cache_clear();
}

function theme_responsive_image_field_formatter_system_settings($vars) {
  extract($vars);

  $header = array(
    t('Breakpoint Name'),
    t('Media Query'),
    t('Weight'),
  );

  foreach ($form as $key => $data) {
    if (is_int($key)) {
      unset($data['breakpoint']['#title'], $data['media_query']['#title'], $data['weight']['#title']);
      $row[0] = drupal_render($data['breakpoint']) . drupal_render($data['machine_name']);
      $row[1] = drupal_render($data['media_query']);
      $row[3] = drupal_render($data['weight']);
      $rows[] = array('data' => $row, 'class' => array('draggable'));
    }
  }
  $table_data = array(
    'header'     => $header,
    'rows'       => $rows,
    'attributes' => array('id' => 'riff-breakpoints-table'),
  );
  $output = theme('table', $table_data) . drupal_render($form['add']) . drupal_render($form['delete']);
  drupal_add_tabledrag('riff-breakpoints-table', 'order', 'sibling', 'breakpoint-weight');

  return $output;
}

/**
 * Exists callback for breakpoint machine names
 */
function responsive_image_field_formatter_unique_machinename($value) {
  $breakpoints = variable_get('riff_breakpoints', array());
  if (!count($breakpoints)) {
    return FALSE;
  }
  foreach ($breakpoints as $breakpoint) {
    if ($breakpoint['machine_name'] == $value) {
      return TRUE;
    }
  }
  return FALSE;
}

function responsive_image_field_formatter_settings_add_remove_breakpoint_callback($form, &$form_state) {
  return $form['riff_breakpoints'];
}

function responsive_image_field_formatter_settings_add_breakpoint($form, &$form_state) {
  $form_state['breakpoint_count']++;
  $form_state['rebuild'] = TRUE;
}

function responsive_image_field_formatter_settings_remove_breakpoint($form, &$form_state) {
  array_pop($form_state['values']['riff_breakpoints']);
  $form_state['breakpoint_count']--;
  if ($form_state['breakpoint_count'] < 1) {
    $form_state['breakpoint_count'] = 0;
  }
  $form_state['rebuild'] = TRUE;
}